#  Copyright (c) 2020 Russell Smiley.
#
#   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
#  associated documentation files (the "Software"), to deal in the Software without restriction,
#  including without limitation the rights to use, copy, modify, merge, publish, distribute,
#  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or
#  substantial portions of the Software.
#
#  You should have received a copy of the MIT License
#  along with the project.  If not, see <https://opensource.org/licenses/MIT>.
#

import abc


class Subcommand(metaclass=abc.ABCMeta):
    """Abstract interface for subcommand parsing."""

    def __init__(self, parent_node=None):
        self.parent = parent_node

    @abc.abstractmethod
    def add_subcommand(self, *args: list, **kwargs: dict):
        pass

    @abc.abstractmethod
    def assign(self, arguments):
        pass
