#  Copyright (c) 2020 Russell Smiley.
#
#   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
#  associated documentation files (the "Software"), to deal in the Software without restriction,
#  including without limitation the rights to use, copy, modify, merge, publish, distribute,
#  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or
#  substantial portions of the Software.
#
#  You should have received a copy of the MIT License
#  along with the project.  If not, see <https://opensource.org/licenses/MIT>.
#


"""Top level command line argument parsing configuration."""

import typing

from .base import BadArgument, CommandTree
from .cluster import CLUSTER_SUBCOMMAND_GRAPH
from .email import EMAIL_SUBCOMMAND_GRAPH
from .group import GROUP_SUBCOMMAND_GRAPH


class UserOptions:
    """Capture user options specified using command line arguments."""

    __DEFAULT_FILE = "contacts.yml"

    __CONTACTSFILE_ARGUMENT_GRAPH = {
        "names": ["--file", "-f",],
        "options": {
            "default": __DEFAULT_FILE,
            "dest": "contactsFile",
            "help": "Contacts file to manage (default "
            "{0}"
            ")".format(__DEFAULT_FILE),
            "type": str,
        },
    }

    __DEBUG_ARGUMENT_GRAPH = {
        "names": ["--debug", "-d",],
        "options": {
            "action": "store_true",
            "help": "Enable exception stack trace reporting to command line",
        },
    }

    __CONTACTS_COMMAND_GRAPH = {
        "arguments": [__CONTACTSFILE_ARGUMENT_GRAPH, __DEBUG_ARGUMENT_GRAPH,],
        "options": {"dest": "activeSubcommand",},
        "subcommands": [
            CLUSTER_SUBCOMMAND_GRAPH,
            EMAIL_SUBCOMMAND_GRAPH,
            GROUP_SUBCOMMAND_GRAPH,
        ],
    }

    def __init__(self):
        self.__command_tree = CommandTree(specification=self.__CONTACTS_COMMAND_GRAPH)
        self.__parsed_arguments = None

    def parse(self, command_line_arguments: typing.List[str]):
        """
        Parse command line arguments.

        :param command_line_arguments:
        :return:
        """
        self.__parsed_arguments = self.__command_tree.parse(command_line_arguments)

        if not self.__parsed_arguments.activeSubcommand:
            raise BadArgument("Must specify a contact list operation")

    def __getattr__(self, item):
        """
        From parsed arguments, recover the items from an `argparse.Namespace` object.

        :param item: parsed argument item name.

        :return: Parsed argument item.
        """
        return getattr(self.__parsed_arguments, item)

    @classmethod
    def from_arguments(cls, command_line_arguments: typing.List[str]):
        options = cls()

        options.parse(command_line_arguments)

        return options
