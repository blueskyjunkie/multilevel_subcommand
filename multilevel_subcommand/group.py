#  Copyright (c) 2020 Russell Smiley.
#
#   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
#  associated documentation files (the "Software"), to deal in the Software without restriction,
#  including without limitation the rights to use, copy, modify, merge, publish, distribute,
#  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or
#  substantial portions of the Software.
#
#  You should have received a copy of the MIT License
#  along with the project.  If not, see <https://opensource.org/licenses/MIT>.
#


GROUP_SUBCOMMAND_GRAPH = {
    "names": ["group",],
    "specification": {
        "arguments": list(),
        "options": {
            "dest": "groupCommand",
            "help": "Add/remove operations",
            "title": "Group management",
        },
        "subcommands": [ADD_GROUP_SUBCOMMAND_GRAPH, RM_GROUP_SUBCOMMAND_GRAPH,],
    },
}

"""
Subcommand graph for `group rm`.
"""

GROUP_ARGUMENT = {
    "names": ["group",],
    "options": {"help": "Group(s) to remove", "nargs": "+", "type": str,},
}

CLUSTER_ARGUMENT = {
    "names": ["--cluster", "-c", "--clusters"],
    "options": {
        "default": list(),
        "dest": "clusters",
        "help": "Cluster(s) from which to remove the group",
        "nargs": "+",
        "type": str,
    },
}

KEEP_ARGUMENT = {
    "names": ["--no-keep", "-n",],
    "options": {
        "action": "store_false",
        "dest": "keep",
        "help": "Remove the group itself from contacts when removing from all clusters. Any email addresses unique "
        "to the group will be lost.",
    },
}

RM_GROUP_SUBCOMMAND_GRAPH = {
    "names": ["rm",],
    "options": {
        "dest": "remove",
        "help": "Remove a group from a selected cluster or clusters, or from all clusters.",
    },
    "specification": {
        "subcommands": list(),
        "arguments": [GROUP_ARGUMENT, CLUSTER_ARGUMENT, KEEP_ARGUMENT,],
    },
}
