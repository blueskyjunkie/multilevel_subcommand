#  Copyright (c) 2020 Russell Smiley.
#
#   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
#  associated documentation files (the "Software"), to deal in the Software without restriction,
#  including without limitation the rights to use, copy, modify, merge, publish, distribute,
#  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or
#  substantial portions of the Software.
#
#  You should have received a copy of the MIT License
#  along with the project.  If not, see <https://opensource.org/licenses/MIT>.
#


"""Command tree construction."""

import argparse
import logging
import typing


log = logging.getLogger( __name__ )


class CommandTreeNode :
    """Construct a command tree node (command) from a specification dictionary."""


    def __init__( self, name, parser,
                  specification = None ) :
        self.specification = specification

        self.name = name
        self.parser = parser
        self.subparsers = None
        self.subcommands = list()

        self.__add_arguments()
        self.__add_subcommands()


    def __add_arguments(self) :
        """Add arguments to the parser."""
        if 'arguments' in self.specification :
            for this_argument in self.specification[ 'arguments' ] :
                log.debug( 'Adding argument, {0}'.format( this_argument[ 'names' ][ 0 ] ) )

                self.parser.add_argument( *this_argument[ 'names' ], **this_argument[ 'options' ] )


    def __add_subcommands(self) :
        """Add sub-commands to the parser."""
        if 'subcommands' in self.specification :
            if self.specification[ 'subcommands' ] :
                if 'options' in self.specification :
                    self.subparsers = self.parser.add_subparsers( **self.specification[ 'options' ] )
                else :
                    self.subparsers = self.parser.add_subparsers()

            for this_subcommand in self.specification[ 'subcommands' ] :
                this_name = this_subcommand[ 'names' ][ 0 ]
                this_specification = this_subcommand[ 'specification' ]

                this_aliases = list()
                if len( this_subcommand[ 'names' ] ) > 1 :
                    this_aliases = this_subcommand[ 'names' ][ 1 : ]

                this_parser = self.subparsers.add_parser( this_name,
                                                         aliases = this_aliases )

                log.debug( 'Adding subcommand, {0}'.format( this_name ) )

                self.subcommands.append( CommandTreeNode( this_name, this_parser,
                                                          specification = this_specification ) )
        else :
            log.debug( 'No subcommands in node' )


class CommandTree( CommandTreeNode ) :
    """Construct the command tree from a specification dictionary."""


    def __init__( self,
                  specification: typing.Optional[typing.Dict[str,typing.Any]]= None ) :
        parser = argparse.ArgumentParser()

        super().__init__( 'top', parser,
                          specification = specification )


    def parse(self, command_line_arguments: typing.List[str]) -> argparse.Namespace :
        return self.parser.parse_args(command_line_arguments)
